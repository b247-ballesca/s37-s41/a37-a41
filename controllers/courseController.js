const Course = require("../models/Course");

module.exports.addCourse = (data) => {

	if(data.isAdmin){

		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		return newCourse.save().then((course, error) => {
			
			if(error){
				return false;
			} else {
				return {
					message: "New course succesfully created!"
				}
			};
		});
	} 

	let message = Promise.resolve({
		message: "User muct be an Admin to access this!"
	})

	return message.then((values) => {
		return values;
	})
	
};

module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result
	});
};

module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result
	});
};

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	});
};

module.exports.updateCourse = (reqParams, reqBody) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.id, updateCourse).then((course, error) => {
		if(error){
			return false
		} else {
			let message = `Succesfully updated Course: "${reqParams.id}"`
			return message 
		}
	});
};

module.exports.updateisActive = (reqParams, reqBody) => {
	let updateisActive = {
		isActive: reqBody.isActive
	};

	return Course.findByIdAndUpdate(reqParams.id, updateisActive).then((course, error) => {
		if(error){
			return false
		} else {
			let message = `Succesfully updated Course: "${reqParams.id}"`
			return message 
		}
	})
}