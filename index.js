const express = require("express");
const mongoose  = require("mongoose");
const cors  = require("cors")

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();

const port = 4004;

mongoose.connect("mongodb+srv://mnballesca0229:09283542019@s36.yo0msap.mongodb.net/?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log(`Now connected to MongoDB Atlas`));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(port, () => console.log(`API is now online on port ${port}`));



